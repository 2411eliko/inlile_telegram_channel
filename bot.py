# This bot allows you to search files from Telegram channels
# in inline mode.
# The bot connect to channels and add any file to DB (a json file).
# and allow specific users to search and manage users and permissions.


from Tele import *
from uuid import uuid4


def make_query(file):
    """Make an query for 'answer_inline_query'"""
    return inline_query_result_document(
        id=str(uuid4()),
        title=file,
        caption=json_db[file]['caption'],
        document_url=json_db[file]['file_id'],
        mime_type=json_db[file]['mime_type']
    )


# Create answer for case no results were found.
NO_RESULTS = InlineQueryResultArticle(
    id=0,
    input_message_content=InputTextMessageContent('😔'),
    title='no results'
)


@bot('document', chat_type='channel')
def doc(update):
    """Adding every new file to DB"""
    if update.chat.id not in allowed_channels:
        return
    document = update.document
    json_db[document.file_name] = {
        'mime_type': document.mime_type,
        'file_id': document.file_id,
        'file_size': document.file_size
    }

    if 'caption' in update:
        json_db[document.file_name]['caption'] = update.caption
    else:
        json_db[document.file_name]['caption'] = ''
    with open('json_db.json', 'w') as n_j:
        json.dump(json_db, n_j,
                  indent=4,
                  ensure_ascii=False)


@bot(chat_type='private', command=('start', 'help'))
def start(update):
    """Guidance for new users Who sent /start or /help"""
    if not check_users(update['from']['id'], level='users'):
        return

    text = '''
    Hi!
    you can search for files from several channels
    via using *inline mode*..
    '''
    reply_markup = [{'search': 'IDM',
                     'type': 'switch_inline_query_current_chat'}]
    if check_users(update['from']['id'], level='special_users'):
        reply_markup.append({'manage': 'manage'})
    message_reply(update, text=text,
                  reply_markup=InlineKeyboard(reply_markup, num_line=2))


@bot(chat_type='private', callback_query='manage')
def manage_query(update):
    """Reacts to the 'manege' button"""
    if not check_users(update['from']['id'], level='special_users'):
        return

    text = '''
    to add users to *allowed_users* list, 
    send the user id in format:
    `add_user: 123456789`
    
    to remove user from *allowed_users* list, 
    send the user id in format:
    `remove_user: 123456789`
    
    to add channel to *channel* list, 
    send the channel id in format:
    `add_channel: 123456789`
    
    to remove channel from *channel* list, 
    send the channel id in format:
    `remove_channel: 123456789`
    '''
    reply_markup = [{'view bot info': 'view_bot_info'}]

    if check_users(update['from']['id'], level='CEO'):
        reply_markup.append({'CEO': 'CEO'})

    message_reply(update, text=text,
                  reply_markup=InlineKeyboard(reply_markup, num_line=1))


@bot(chat_type='private', callback_query='CEO')
def ceo_query(update):
    """Reacts to the 'CEO' button"""
    if not check_users(update['from']['id'], level='CEO'):
        return
    text = '''
    to add *special_user*, send the user id in format:
    `add_special_user: 123456789`
    
    to remove *special_user*, send the user id in format:
    `remove_special_user: 123456789`
    '''
    message_reply(update, text=text)


@bot(chat_type='private', callback_query='view_bot_info')
def info(update):
    """Reacts to the 'view_bot_info' button"""
    if not check_users(update['from']['id'], level='special_users'):
        return
    text = '*Users*:\n`{}`\n\n*Channels*:\n`{}`'.format(
        json.dumps(allowed_users, indent=4, ensure_ascii=False),
        str(allowed_channels)
    )

    message_reply(update, text=text)


@bot(chat_type='private', regex='^add_user:')
def add_user(update):
    if not check_users(update['from']['id'], level='special_users'):
        return
    
    if int(update.text[10:]) in allowed_users['users']:
        return message_reply(update, '*user already exist..*')

    chat = getChat(update.text[10:])
    if chat['ok'] and chat['result']['type'] == 'private':
        allowed_users['users'].append(int(update.text[10:]))

        with open('allowed_users.json', 'w') as n_j:
            json.dump(allowed_users, n_j,
                      indent=4,
                      ensure_ascii=False)

        message_reply(update, '*user added successfully*')

    else:
        message_reply(update, '*user not found*')


@bot(chat_type='private', regex='^remove_user:')
def remove_user(update):
    if not check_users(update['from']['id'], level='special_users'):
        return
    
    if int(update.text[12:]) not in allowed_users['users']:
        return message_reply(update, '*the user not exist..*')

    if int(update.text[12:]) in allowed_users['special_users']:
        if not check_users(update['from']['id'], level='CEO'):
            return message_reply(update, '*the user is special_user.. 🤷‍♂*')
    allowed_users['users'].remove(int(update.text[12:]))

    with open('allowed_users.json', 'w') as n_j:
        json.dump(allowed_users, n_j,
                  indent=4,
                  ensure_ascii=False)
    message_reply(update, '*user kicked successfully*')


@bot(chat_type='private', regex='^add_channel:')
def add_channel(update):
    if not check_users(update['from']['id'], level='special_users'):
        return
    if int(update.text[13:]) in allowed_channels:
        return message_reply(update, '*channel already exist..*')

    chat = getChat(update.text[13:])
    if chat['ok'] and chat['result']['type'] == 'channel':
        allowed_channels.append(int(update.text[13:]))
        with open('allowed_channels.txt', 'w') as ch:
            for item in allowed_channels:
                ch.write("%s\n" % item)
        message_reply(update, '*channel added successfully*')

    else:
        message_reply(update, '*channel not found*')


@bot(chat_type='private', regex='^remove_channel:')
def remove_channel(update):
    if not check_users(update['from']['id'], level='special_users'):
        return
    if int(update.text[15:]) not in allowed_channels:
        message_reply(update, '*channel not exist in the list..*')
    with open('allowed_channels.txt', 'w') as ch:
        for item in allowed_channels:
            ch.write("%s\n" % item)
    message_reply(update, '*channel removed successfully*')


@bot(chat_type='private', regex='^add_special_user:')
def add_special_user(update):
    if not check_users(update['from']['id'], level='CEO'):
        return

    if int(update.text[18:]) in allowed_users['special_users']:
        return message_reply(update, '*user already exist..*')

    chat = getChat(update.text[18:])
    if chat['ok'] and chat['result']['type'] == 'private':
        allowed_users['special_users'].append(int(update.text[18:]))

        with open('allowed_users.json', 'w') as n_j:
            json.dump(allowed_users, n_j,
                      indent=4,
                      ensure_ascii=False)

        message_reply(update, '*special_users added successfully*')

    else:
        message_reply(update, '*user not found*')


@bot(chat_type='private', regex='^remove_special_user:')
def remove_special_user(update):
    if not check_users(update['from']['id'], level='ECO'):
        return
    if int(update.text[21:]) not in allowed_users['special_users']:
        message_reply(update, '*the user not in special_users..*')

    allowed_users['special_users'].remove(int(update.text[21:]))

    with open('allowed_users.json', 'w') as n_j:
        json.dump(allowed_users, n_j,
                  indent=4,
                  ensure_ascii=False)
    message_reply(update, '*special_users removed successfully*')


@bot('inline_query')
def inline(update):
    if not check_users(update['from']['id'], level='users'):
        return

    query = update.query
    # if the user search for a specific file
    if query:
        results = []
        for file in json_db:
            if query in file or query in json_db[file]['caption']:
                results.append(make_query(file))
    else:
        results = [make_query(file) for file in json_db]

    if not results:
        results = [NO_RESULTS]

    answer_inline_query(update, results, cache_time=0)


def check_users(user_id, level):
    """
    :param user_id: user user_id from the message sent
    :param level: permission group e.g 'CEO', 'special_users', 'users'.
    :return: bool
    """
    if user_id in allowed_users[level]:
        return True
    return False


# DB of files, users and channels
try:
    with open('json_db.json', 'r') as j:
        json_db = json.load(j)
except FileNotFoundError:
    json_db = {}

try:
    with open('allowed_users.json', 'r') as j:
        allowed_users = json.load(j)
except FileNotFoundError:
    allowed_users = {'CEO': [],
                     'special_users': [],
                     'users': []}

try:
    with open('allowed_channels.txt', 'r') as j:
        allowed_channels = [int(line.rstrip('\n')) for line in j]
except FileNotFoundError:
    allowed_channels = []

# default administrator list
CEO = [120008108]


allowed_users['CEO'].extend(CEO)
allowed_users['special_users'].extend(allowed_users['CEO'])
allowed_users['users'].extend(allowed_users['special_users'])

# delete duplicate items
for key in allowed_users:
    allowed_users[key] = list(set(allowed_users[key]))

account('TOKEN')
bot_run(multi=True, timeout=60)
